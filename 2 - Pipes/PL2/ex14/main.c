#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>

int main(){

	pid_t pid;
	int p[2];
	char line[10];

	if(pipe(p)==-1){
		perror("ERROR - Pipe creation failed");
		return 0;
	}

	pid = fork();
	if(pid == -1) {
		perror("ERROR - Process creation failed");

	} else if (pid==0) {

		close(p[1]);
		dup2(p[0], 0);
		close(p[0]);

		execlp("more", "more", "-10", "-d", NULL); // presents the results in pages of 10 lines, instead of using the current size of the terminal as a measure
		perror("ERROR - Exec failed");
		exit(EXIT_FAILURE);

	} else if(pid > 0) {

		close (p[0]);

		int i;
		char number[5];

		for(i = 0; i < 100; i++){
			strcpy(line, "Line ");
			sprintf(number, "%d\n", i+1); // casts an int to a char/string
			strcat(line, number);	// merges two strings together
			write(p[1], line, strlen(line));
		}

		close(p[1]);
		wait(NULL);
	}
	return 0;
}
