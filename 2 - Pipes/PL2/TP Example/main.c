#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>

int main() {
	int p[2];
	pid_t pid;
	pipe(p);
	
	pid = fork();
	if(pid > 0) {
		close(p[0]);
		dup2(p[1], 1);
		close(p[1]);
		execlp("/bin/ls", "ls", NULL);
		wait(NULL);
	} else if(pid == 0) {
		close(p[1]);
		dup2(p[0], 0);
		close(p[0]);
		execlp("/bin/sort", "sort", NULL);
		exit(0);
	} else if(pid == -1) {
		perror("Error");
	}

	return 0;	
}

