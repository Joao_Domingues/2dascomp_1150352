#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

typedef struct {
  int product_code;
  char * name;
  double price;
} product;

typedef struct {
  int index;
  int product_code;
} request;

/*
  Child 0 reads from answers[0][0] and writes on requests[1]
  Child 1 reads from answers[1][0] and writes on requests[1]
  Child 2 reads from answers[2][0] and writes on requests[1]
  Child 3 reads from answers[3][0] and writes on requests[1]
  Child 4 reads from answers[4][0] and writes on requests[1]
  Father  reads from requests[0] and writes on answers[0-4][1]
*/

int main() {
  int DATABASE_SIZE = 5, EXIT_VALUE = 1000;
  int requests[2], answers[5][2];

  if(pipe(requests) == -1 || pipe(answers[0]) == -1 || pipe(answers[1]) == -1 || pipe(answers[2]) == -1 || pipe(answers[3]) == -1 || pipe(answers[4]) == -1) {
    perror("ERROR - pipe creation failed");
    exit(0);
  }

  int i, j;
  pid_t pid;

  for(i = 0; i < 5; i++) {
    pid = fork();
    if(pid == 0) {
      for(j = 0; j < 5; j++) { // closes all pipe ends but the ones specified in the initial comment
        if(j != i) close(answers[j][0]);
        close(answers[j][1]);
        close(requests[0]);
      }
      break;
    } else if(pid == -1) {
      perror("ERROR - Process creation failed");
      exit(0);
    }
  }

  if(i == 5) {
    for(j = 0; j < 5; j++) {
      close(answers[j][0]);
    }
    close(requests[1]);

    product database[DATABASE_SIZE];

    product p1;
    p1.product_code = 1;
    p1.name = "Yogurt";
    p1.price = 1.99;
    database[0] = p1;

    p1.product_code = 2;
    p1.name = "Apples";
    p1.price = 2.50;
    database[1] = p1;

    p1.product_code = 3;
    p1.name = "Lettuce";
    p1.price = 3.75;
    database[2] = p1;

    p1.product_code = 4;
    p1.name = "Fish";
    p1.price = 6.99;
    database[3] = p1;

    p1.product_code = 5;
    p1.name = "Meat";
    p1.price = 5.25;
    database[4] = p1;

    request r1;
    int index, found, canContinue;
    while(1) {  // infinite cycle, asking for what barcode to be used
      found = 0;

      printf("Select barcode reader (0-%d) to use (select %d to exit)\n", DATABASE_SIZE-1, EXIT_VALUE);
      scanf("%d", &index);

      if(index < 5 && index >= 0) { // validates the number of the barcode reader

        write(answers[index][1], &index, sizeof(int));    // pings the chosen barcode reader
        read(requests[0], &r1, sizeof(request));          // waits for scanned product code
        for(j = 0; j < DATABASE_SIZE; j++) {
          if(database[j].product_code == r1.product_code) {
            found = 1;
            p1 = database[j];
          }
        }

        if(!found) {
          p1.product_code = r1.product_code;
          p1.name = "Product not found";
          p1.price = 0.0;
        }

        write(answers[r1.index][1], &p1, sizeof(product));  // sends product info back to the barcode reader
        read(requests[0], &canContinue, sizeof(int)); // allows the program to continue

      } else if(index == EXIT_VALUE) {
        printf("Pinging all barcodes to shut down\n");
        for(j = 0; j < 5; j++) write(answers[j][1], &index, sizeof(int));  // pings all the barcode readers to end
        break;
      } else {
        printf("This barcode reader isn't available, please select another one.\n");
      }

    }

    printf("Shutting down database\n");
    for(j = 0; j < 5; j++) {
      close(answers[j][1]);
    }
    close(requests[0]);

  } else {

    request r1;
    product p1;
    int canContinue = 1;

    while(1) {
      read(answers[i][0], &r1.index, sizeof(int)); // waits for a ping from the database

      if(r1.index == EXIT_VALUE) {
        printf("Barcode Reader %d turning off.\n", i);
        close(answers[i][0]);
        close(requests[1]);
        exit(0);
      }

      printf("Please scan the barcode on the product (insert number):\n");
      scanf("%d", &r1.product_code);

      write(requests[1], &r1, sizeof(request));

      read(answers[i][0], &p1, sizeof(product));

      printf("\n-----Product Info-----\nName: %s\nPrice: %.2f€\nCode: %d\n----------------------\n\n", p1.name, p1.price, p1.product_code);

      write(requests[1], &canContinue, sizeof(int));
    }
  }

  return 0;
}
