#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

int main() {
	int vec1[1000], vec2[1000];		/* arrays to lookup */

	int i;

	/* needed to initialize random number generator (RNG) */
	time_t t;

	/* intializes RNG (srand():stdlib.h; time(): time.h) */
	srand ((unsigned) time (&t));

	/* initialize array with random numbers (rand(): stdlib.h) */
	for (i = 0; i < 1000; i++) {
		vec1[i] = rand () % 10;
		vec2[i] = rand () % 10;
		/* se for usado o código abaixo em vez do de cima, o resultado final dá 2 em cada uma das posições do vectotal, como esperado
		vec1[i] = 1;
		vec2[i] = 1;
		*/
	}

	// --------------

	int p1[2], p2[2], p3[2], p4[2], p5[2];
	int vectmp[200];
	int j, h = 0;
	pid_t pid;

	if(pipe(p1) == -1) {
		perror("ERROR");
		return 1;
	}
	if(pipe(p2) == -1) {
		perror("ERROR");
		return 1;
	}
	if(pipe(p3) == -1) {
		perror("ERROR");
		return 1;
	}
	if(pipe(p4) == -1) {
		perror("ERROR");
		return 1;
	}
	if(pipe(p5) == -1) {
		perror("ERROR");
		return 1;
	}

	for(i = 0; i < 5; i++) {
			pid = fork();
			if(pid == 0) {
				break;
			} else if(pid == -1){
				perror("ERROR");
			}
	}

	if(i == 0) {
		close(p1[0]);

		for(j = i*200; j < (i+1)*200; j++) { // cycles through the vectors and sums the values
			vectmp[h] = vec1[j] + vec2[j];
			h++;
		}

		write(p1[1], vectmp, 200*sizeof(int));
		close(p1[1]);
		exit(0);

	} else if(i == 1) {
		close(p2[0]);

		for(j = i*200; j < (i+1)*200; j++) { // cycles through the vectors and sums the values
			vectmp[h] = vec1[j] + vec2[j];
			h++;
		}

		write(p2[1], vectmp, 200*sizeof(int));
		close(p2[1]);
		exit(0);

	} else if(i == 2) {
		close(p3[0]);

		for(j = i*200; j < (i+1)*200; j++) { // cycles through the vectors and sums the values
			vectmp[h] = vec1[j] + vec2[j];
			h++;
		}

		write(p3[1], vectmp, 200*sizeof(int));
		close(p3[1]);
		exit(0);

	} else if(i == 3) {
		close(p4[0]);

		for(j = i*200; j < (i+1)*200; j++) { // cycles through the vectors and sums the values
			vectmp[h] = vec1[j] + vec2[j];
			h++;
		}

		write(p4[1], vectmp, 200*sizeof(int));
		close(p4[1]);
		exit(0);

	} else if(i == 4) {
		close(p5[0]);

		for(j = i*200; j < (i+1)*200; j++) { // cycles through the vectors and sums the values
			vectmp[h] = vec1[j] + vec2[j];
			h++;
		}

		write(p5[1], vectmp, 200*sizeof(int));
		close(p5[1]);
		exit(0);

	} else if(i == 5) {
		close(p1[1]);
		close(p2[1]);
		close(p3[1]);
		close(p4[1]);
		close(p5[1]);

		int vectotal[1000], k;

		read(p1[0], vectmp, 200*sizeof(int));
		close(p1[0]);
		for(h = 0; h < 200; h++) {
			vectotal[h] = vectmp[h];
		}

		read(p2[0], vectmp, 200*sizeof(int));
		close(p2[0]);
		k = 0;
		for(h = 200; h < 400; h++) {
			vectotal[h] = vectmp[k];
			k++;
		}

		read(p3[0], vectmp, 200*sizeof(int));
		close(p3[0]);
		k = 0;
		for(h = 400; h < 600; h++) {
			vectotal[h] = vectmp[k];
			k++;
		}

		read(p4[0], vectmp, 200*sizeof(int));
		close(p4[0]);
		k = 0;
		for(h = 600; h < 800; h++) {
			vectotal[h] = vectmp[k];
			k++;
		}

		read(p5[0], vectmp, 200*sizeof(int));
		close(p5[0]);
		k = 0;
		for(h = 800; h < 1000; h++) {
			vectotal[h] = vectmp[k];
			k++;
		}

		for(h = 0; h < 1000; h++) {
			printf("Index %d: %d\n", h, vectotal[h]);
		}
	}

	return 0;
}
