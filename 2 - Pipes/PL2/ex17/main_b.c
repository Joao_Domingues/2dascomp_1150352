#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
	char id[30];
	char password[30];
} user;

void setupDatabase(user * users);
int validate(user u1, user * users);

int main() {

	int fatherWrite[2], childWrite[2], valid;

	if(pipe(fatherWrite) == -1 || pipe(childWrite) == -1) {
		perror("ERROR - Pipe creation failed");
		exit(1);
	}

	char * EXIT_COMMAND = "exit";
	user *users = malloc (sizeof (user) * 6);
	setupDatabase(users);

	switch(fork()) {
		case 0:
			close(fatherWrite[1]);
			close(childWrite[0]);

			user u2;

			while(1) {
				read(fatherWrite[0], &u2, sizeof(user));

				if(strcmp(u2.id, EXIT_COMMAND) == 0) {
					puts("Child message - Exit command inserted\n");
					exit(0);
				}

				valid = validate(u2, users);
				printf("valid -> %d\n", valid);

				write(childWrite[1], &valid, sizeof(int));
			}
			perror("ERROR - Child ended cycle");
			break;

		case -1:
			perror("ERROR - Process creation failed");
			exit(1);
			break;

		default: // father process
			close(fatherWrite[0]);
			close(childWrite[1]);
			user u1;

			while(1) {
				printf("\n-----Login-----\nUsername: (write 'exit' to exit)\n> ");
				scanf("%s", u1.id);
				u1.id[strlen(u1.id)] = '\0';

				if(strcmp(u1.id, EXIT_COMMAND) == 0) {
					printf("\nFather message - Exit command inserted\n");
					write(fatherWrite[1], &u1, sizeof(user));
					exit(0);
				}

				printf("Password:\n> ");
				scanf("%s", u1.password);
				u1.password[strlen(u1.password)] = '\0';

				write(fatherWrite[1], &u1, sizeof(user));

				read(childWrite[0], &valid, sizeof(int));

				printf("\nServer Response -> ");
				if(valid == 0) {
					printf("Invalid password\n");
				} else if(valid == 1) {
					printf("Password verified\n");
				} else if(valid == -1) {
					printf("No such user found\n\n");
				}
			}
			perror("ERROR - Father ended cycle");
			break;
	}

	close(fatherWrite[1]);
	close(childWrite[0]);

	return 0;
}

void setupDatabase(user * users) {
	user u1;

	strcpy(u1.id, "user1");
	strcpy(u1.password, "password1");
	*(users) = u1;

	strcpy(u1.id, "test");
	strcpy(u1.password, "pass");
	*(users+1*sizeof(user)) = u1;

	strcpy(u1.id, "admin");
	strcpy(u1.password, "adminpass");
	*(users+2*sizeof(user)) = u1;

	strcpy(u1.id, "1150352");
	strcpy(u1.password, "isep");
	*(users+3*sizeof(user)) = u1;

	strcpy(u1.id, "isep");
	strcpy(u1.password, "lei");
	*(users+4*sizeof(user)) = u1;

	strcpy(u1.id, "scomp");
	strcpy(u1.password, "credits");
	*(users+5*sizeof(user)) = u1;

}

int validate(user u1, user * users) {
	int i;
	user u2;

	for(i = 0; i < 6; i++) {
		u2 = *(users+i*sizeof(user));
		if(strcmp(u1.id, u2.id) == 0) {
			if(strcmp(u1.password, u2.password) == 0) {
				return 1;
			} else {
				return 0;
			}
		}
	}

	return -1;
}
