#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

int main() {

  int p[2], STRING_SIZE = 255;
  if(pipe(p) == -1) perror("ERROR - Pipe creation failed");

  pid_t pid = fork();
  if(pid == -1) {
    perror("ERROR - Process creation failed");
  } else if(pid == 0) {
    close(p[0]);

    dup2(p[1], STDOUT_FILENO);
    close(p[1]);

    execlp("sort", "sort", "fx.txt", (char *) NULL);
    perror("ERROR - Exec failed");
		exit(EXIT_FAILURE);

  } else if(pid > 0) {

    close(p[1]);

    char * str = malloc(STRING_SIZE); // aloca o espaço da memória já prenchido a 0's, para sinalizar o final da string
    read(p[0], str, STRING_SIZE);

    printf("Sorted File Text:\n%s\n", str);

    close(p[0]);

  }

  return(0);
}
