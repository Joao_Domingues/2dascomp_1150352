#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

struct obj {
  int i;
	char * str;
};

int main() {
	int pids[10], p[2];
	pid_t pid;
  int i;
  struct obj o1;
  o1.str = "Win";

  if(pipe(p) == -1) {
    perror("ERROR");
    exit(0);
  }

  for(i = 0; i < 10; i++) {
    pid = fork();
    if(pid == 0) {
      close(p[1]);
      break;
    } else if(pid > 0) {
      pids[i] = pid;
    } else if(pid == -1) {
      perror("ERROR");
    }
  }

  if(pid == 0) {

    read(p[0], &o1, 12);
    close(p[0]);

    printf("I won!\n");

    exit(o1.i);

  } else if(pid > 0) {
    close(p[0]);

    for(i = 0; i < 10; i++) {
      sleep(2);
  		o1.i = i;
      write(p[1], &o1, 12);
    }
    close(p[1]);

    int status;

    for(i = 0; i < 10; i++) {
      waitpid(pids[i], &status, 0);
      printf("PID: %d - Won round: %d\n", pids[i], WEXITSTATUS(status)+1);
    }
  }

	return 0;
}
