#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main() {

	int ls_sort[2], sort_wc[2], i;
	pid_t pid;

	if(pipe(ls_sort) == -1 || pipe(sort_wc) == -1) {
		perror("ERROR - Pipe creation failed");
		exit(1);
	}

	for(i = 0; i < 2; i++) {
		pid = fork();
		if(pid == 0) {
			break;
		} else if(pid == -1) {
			perror("ERROR - Process creation failed");
		}
	}

	switch(i) {
		case 0:
			close(ls_sort[0]);
			close(sort_wc[0]);
			close(sort_wc[1]);

			dup2(ls_sort[1], 1);
			close(ls_sort[1]);

			execlp("ls", "ls", "-la", NULL);
			break;

		case 1:
			close(ls_sort[1]);
			close(sort_wc[0]);

			dup2(ls_sort[0], 0);
			close(ls_sort[0]);

			dup2(sort_wc[1], 1);
			close(sort_wc[1]);
			execlp("sort", "sort", NULL);
			break;

		default: // father process
			close(sort_wc[1]);
			close(ls_sort[0]);
			close(ls_sort[1]);

			dup2(sort_wc[0], 0);
			close(sort_wc[0]);

			execlp("wc", "wc", "-l", NULL);
			break;
	}

	return 0;
}
