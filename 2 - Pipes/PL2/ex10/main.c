#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

int main() {

	int p[2], p2[2];
	pid_t pid;

  if(pipe(p) == -1) {
    perror("ERROR");
    exit(0);
  }

  if(pipe(p2) == -1) {
    close(p[0]);
    close(p[1]);
    perror("ERROR");
    exit(0);
  }

  time_t t;
  srand ((unsigned) time (&t));

  pid=fork();
  if(pid == -1) {
    perror("ERROR");
    exit(0);
  }

  if(pid == 0) {
    close(p[0]);
    close(p2[1]);

    int nextRound, bet, credit;

    printf("CHILD: waiting for nextRound value\n");
    read(p2[0], &nextRound, sizeof(int));
    printf("CHILD: received nextRound: %d \n", nextRound);

    while(nextRound != 0){
      bet = (rand () % 4) + 1;
      write(p[1], &bet, sizeof(int));
      printf("CHILD: sent bet: %d \n", bet);

      printf("CHILD: waiting for credit value\n");
      read(p2[0], &credit, sizeof(int));
      printf("CHILD: received credit: %d \n", credit);

      printf("CHILD: waiting for nextRound value\n");
      read(p2[0], &nextRound, sizeof(int));
      printf("CHILD: received nextRound: %d \n", nextRound);
    }

    close(p[1]);
    close(p2[0]);

    exit(0);

  } else if(pid > 0) {

    close(p[1]);
    close(p2[0]);

    int credit = 20, number, bet, nextRound = 1;

    while(credit > 0) {
      puts("\n-----New Round------");
      write(p2[1], &nextRound, sizeof(int));
      printf("FATHER: sent netRound: %d\n", nextRound);

      number = (rand () % 4) + 1;

      printf("FATHER: waiting for bet value\n");
      read(p[0], &bet, sizeof(int));
      printf("FATHER: received bet: %d\n", bet);

      if(number == bet) {
        printf("Correct! -> Number: %d | Bet: %d\n", number, bet);
        credit = credit + 10;
      } else {
        printf("Wrong!   -> Number: %d | Bet: %d\n", number, bet);
        credit = credit -5;
      }

      write(p2[1], &credit, sizeof(int));
      printf("FATHER: sent credit: %d\n", credit);

      sleep(1);
    }
    nextRound = 0;
    write(p[1], &nextRound, sizeof(int));
    close(p[0]);
    close(p2[1]);
    wait(NULL);

  }
  printf("Game ended!\n");
	return 0;
}
