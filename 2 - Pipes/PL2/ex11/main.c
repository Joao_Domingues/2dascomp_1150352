#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

/*
  Child 0 reads from pipes[0][0] and writes on pipes[1][1]
  Child 1 reads from pipes[1][0] and writes on pipes[2][1]
  Child 2 reads from pipes[2][0] and writes on pipes[3][1]
  Child 3 reads from pipes[3][0] and writes on pipes[4][1]
  Child 4 reads from pipes[4][0] and writes on pipes[5][1]
  Father  reads from pipes[5][0] and writes on pipes[0][1]
*/

int main() {

  int i, j, pipes[6][2];
  if(pipe(pipes[0]) == -1 || pipe(pipes[1]) == -1 || pipe(pipes[2]) == -1 || pipe(pipes[3]) == -1 || pipe(pipes[4]) == -1 || pipe(pipes[5]) == -1) {
    perror("ERROR - Pipe creation failed");
    return(0);
  }

  pid_t pid;
  for(i = 0; i < 5; i++) {
    pid = fork();
    if(pid == -1) {
      perror("ERROR - Process creation failed");
      exit(0);
    } else if(pid == 0) {
      for(j = 0; j < 5; j++) { // closes all pipe ends but the ones specified in the initial comment
        if(j != i) close(pipes[j][0]);
        if(j != i+1) close(pipes[j][1]);
      }
      break;
    }
  }

  time_t t;
  int toSend, received;

  if(i < 5) {  // children

    read(pipes[i][0], &received, sizeof(int));
    sleep(1);
    srand ((unsigned) time (&t));
    toSend = (rand () % 500) + 1;         // random number generation
    printf("Child %d - generated number: %d\n", i, toSend);

    if(toSend > received) {
      write(pipes[i+1][1], &toSend, sizeof(int));
    } else {
      write(pipes[i+1][1], &received, sizeof(int));
    }

    close(pipes[i][0]);
    close(pipes[i+1][1]);
    exit(0);

  } else if(i == 5) { // father
    for(j = 0; j < 5; j++) {
      if(j != i) close(pipes[j][0]);
      if(j != 0) close(pipes[j][1]);
    }

    srand ((unsigned) time (&t));
    toSend = (rand () % 500) + 1;
    printf("Father - generated number: %d\n", toSend);
    write(pipes[0][1], &toSend, sizeof(int));

    read(pipes[i][0], &received, sizeof(int));
    printf("Highest number generated: %d\n", received);

    close(pipes[i][0]);
    close(pipes[0][1]);
  }

	return 0;
}
