#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main() {

	int p1[2], p2[2], number, i, result;

	if(pipe(p1) == -1 || pipe(p2) == -1) {
		perror("ERROR - Pipe creation failed");
		exit(1);
	}

	switch(fork()) {
		case 0:
			close(p1[1]);
			close(p2[0]);

			read(p1[0], &number, sizeof(int));

			result = number;
			for(i = number-1; i > 0; i--) result = result * i;
			write(p2[1], &result, sizeof(int));

			close(p2[1]);
			close(p1[0]);
			break;

		case -1:
			perror("ERROR - Process creation failed");
			exit(1);

		default:
			close(p2[1]);
			close(p1[0]);

			printf("Insert a number\n");
			scanf("%d", &number);
			write(p1[1], &number, sizeof(int));

			read(p2[0], &result, sizeof(int));
			printf("Final result of %d! = %d\n", number, result);

			close(p1[1]);
			close(p2[0]);
			break;
	}

	return 0;
}

/* Program without the use of child processes
int main(){

	int result, number, i;

	printf("Insert a number\n");
	scanf("%d", &number);
	result = number;

	for(i = number-1; i > 0; i--) result = result * i;
	printf("Final result of %d! = %d\n", number, result);

	return 0;
}
*/
