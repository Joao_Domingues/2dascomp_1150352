#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

typedef struct {
  int customer_code;
  int product_code;
	int quantity;
} sale;

int main() {
  sale sales[50000];
  sale o1;

	int i;

	/* needed to initialize random number generator (RNG) */
	time_t t;

	/* intializes RNG (srand():stdlib.h; time(): time.h) */
	srand ((unsigned) time (&t));

	/* initialize array with random numbers (rand(): stdlib.h) */
	for (i = 0; i < 50000; i++) {
    o1.customer_code = rand () % 1000;
    o1.product_code = i;
    o1.quantity = rand () % 101;
		sales[i] = o1;
	}

	// --------------

	int p[2], j, counter = 0;
	pid_t pid;

  if(pipe(p) == -1) {
    perror("ERROR");
    exit(0);
  }

  for(i = 0; i < 10; i++) {
    pid = fork();
    if(pid == 0) {
      close(p[0]);
      break;
    } else if(pid == -1) {
      perror("ERROR");
    }
  }

  if(pid == 0) {

    for(j = i*5000; j < (i+1)*5000; j++) {
      o1 = sales[j];
      if(o1.quantity >= 100) { // nota para o prof: aumentei este valor de 20 para 100, para diminuir o volume de resultados e ser mais fácil de visualizar
        counter++;
        write(p[1], &(o1.product_code), 4);
        printf("Product code: %d | Quantity: %d\n", o1.product_code, o1.quantity);
      }
    }
    close(p[1]);
    exit(counter);

  } else if(pid > 0) {
    close(p[1]);

    int status;
    for(i = 0; i < 10; i++) {
      wait(&status);
      counter += WEXITSTATUS(status);
    }

    int products[counter], product;
    for(i = 0; i < counter; i++) {
      read(p[0], &product, 4);
      products[i] = product;
      printf("Product: %d\n", products[i]);
    }
    close(p[0]);
    printf("Number of products found: %d\n", counter);
  }

	return 0;
}
