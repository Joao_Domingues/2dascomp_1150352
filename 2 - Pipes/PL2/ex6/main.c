#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

int main() {
	int vec1[1000], vec2[1000];		/* arrays to lookup */

	int i;

	/* needed to initialize random number generator (RNG) */
	time_t t;

	/* intializes RNG (srand():stdlib.h; time(): time.h) */
	srand ((unsigned) time (&t));

	/* initialize array with random numbers (rand(): stdlib.h) */
	for (i = 0; i < 1000; i++) {
		vec1[i] = rand () % 10;
		vec2[i] = rand () % 10;
		/* se for usado o código abaixo em vez do de cima, o resultado final dá 2000, como esperado (1000 posições * 2 vetores = 2000)
		vec1[i] = 1;
		vec2[i] = 1;
		*/
	}

	// --------------

	int p[2];
	pid_t pid;

	if(pipe(p) == -1) {
		perror("ERROR");
		return 1;
	}

	for(i = 0; i < 5; i++) {
			pid = fork();
			if(pid == 0) {
				break;
			} else if(pid == -1){
				perror("ERROR");
			}
	}

	if(pid > 0) {
		close(p[1]);		// closes unnecessary descriptor
		int total = 0, tmp;

		read(p[0], &tmp, sizeof(int));	// read tmp sent by children
		total += tmp;
		read(p[0], &tmp, sizeof(int));
		total += tmp;
		read(p[0], &tmp, sizeof(int));
		total += tmp;
		read(p[0], &tmp, sizeof(int));
		total += tmp;
		read(p[0], &tmp, sizeof(int));
		total += tmp;
		close(p[0]);		// closes unnecessary descriptor

		printf("Total sum of elements: %d\n", total);

		wait(NULL);

	} else if(pid == 0) {

		close(p[0]);		// closes unnecessary descriptor

		int tmp = 0, j;

		for(j = i*200; j < (i+1)*200; j++) { // cycles through the vectors and sums the values
			tmp += vec1[j] + vec2[j];
		}

		write(p[1], &tmp, sizeof(int));	// sends tmp to parent
		close(p[1]);	// closes unnecessary descriptor

		exit(0);
	} else if(pid == -1) {
		perror("Error");
	}

	return 0;
}
