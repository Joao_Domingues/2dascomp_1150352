#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>

int main() {
	int p[2];
	pid_t pid;
	pipe(p);
	int STRING_SIZE = 255;
	char string[STRING_SIZE];
	
	pid = fork();
	if(pid > 0) {
		close(p[0]);
		int i = 0;
		FILE *fp;							// declare file variable
		fp = fopen("file.txt", "r");		// open file in read mode
		while(!feof(fp)) {					// while EOF has not been reached
			string[i] = (char) fgetc(fp);	// cycle through the file's characters
			i++;							// increment the position in the string
		}
		fclose(fp);							// close file
		write(p[1], string, i);
		close(p[1]);
		wait(NULL);
	} else if(pid == 0) {
		close(p[1]);
		int n = read(p[0], string, STRING_SIZE);
		printf("%d", n);
		string[n-1] = 0;
		printf("%s\n", string);
		close(p[0]);
		exit(0);
	} else if(pid == -1) {
		perror("Error");
	}
	
	return 0;	
}

