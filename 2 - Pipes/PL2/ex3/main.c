#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>

int main() {
	int p[2], status;
	pid_t pid;
	pipe(p);
	
	pid = fork();
	if(pid > 0) {
		close(p[0]);		
		write(p[1], "Hello World", 12);
		write(p[1], "Goodbye!", 9);
		close(p[1]);
		wait(&status);
		printf("\nChild process (pid:%d) ended with exit status: %d\n", pid, WEXITSTATUS(status));
	} else if(pid == 0) {
		char str1[12];
		char str2[9];
		
		close(p[1]);
		read(p[0], str1, 12);
		printf("%s\n", str1);
		read(p[0], str2, 9);
		printf("%s\n", str2);
		close(p[0]);
		exit(0);
	} else if(pid == -1) {
		perror("Error");
	}
	
	return 0;	
}

