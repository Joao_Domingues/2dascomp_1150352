#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>

struct obj {
	int x;	
	char string[10];
};

int main() {
	int x, p[2];
	pid_t pid;
	pipe(p);
	int STRING_SIZE = 10;
	char string[STRING_SIZE];
	
	pid = fork();
	/*
	// alínea a)
	if(pid > 0) {
		close(p[0]);
		fgets(string, STRING_SIZE, stdin); // max 9 chars
		scanf("%d", &x);		
		write(p[1], string, STRING_SIZE);
		write(p[1], &x, 4);
		close(p[1]);
		wait(NULL);
	} else if(pid == 0) {
		close(p[1]);
		read(p[0], string, STRING_SIZE);
		printf("print do filho: %s\n", string);
		read(p[0], &x, sizeof(int));
		printf("print do filho: %d\n", x);
		close(p[0]);
		exit(0);
	} else if(pid == -1) {
		perror("Error");
	}
	*/
	
	// alínea b)
	if(pid > 0) {
		close(p[0]);
		fgets(string, STRING_SIZE, stdin); // max 9 chars
		scanf("%d", &x);	
		struct obj o1;
		strcpy(o1.string, string);
		o1.x = x;
		write(p[1], &o1, 14);
		close(p[1]);
		wait(NULL);
	} else if(pid == 0) {
		close(p[1]);
		read(p[0], &x, sizeof(int));
		printf("print do filho: %d\n", x);
		read(p[0], string, STRING_SIZE);
		printf("print do filho: %s\n", string);
		close(p[0]);
		exit(0);
	} else if(pid == -1) {
		perror("Error");
	}

	return 0;	
}

