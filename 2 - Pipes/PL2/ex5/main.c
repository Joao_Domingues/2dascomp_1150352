#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/wait.h>
#include <stdlib.h>

int main() {
	int up[2], down[2];
	pid_t pid;
	
	if(pipe(up) == -1) {
		perror("ERROR");
		return 1;
	}

	if(pipe(down) == -1) {
		close(up[0]);
		close(up[1]);
		perror("ERROR");
		return 1;
	}

	pid = fork();
	if(pid > 0) {
		close(down[0]);		// closes unnecessary descriptor
		close(up[1]);			// closes unnecessary descriptor

		char str1[255];

		read(up[0], str1, 255);	// read message sent by the other process
		close(up[0]);		// closes unnecessary descriptor
		printf("SERVER: Message received from Client: %s\n", str1);

		int i = 0;
		while(str1[i]) { // cycles through the string
			if(isupper(str1[i])) { // checks if a character is upper or lower case, changing accordingly
				str1[i] = (char) tolower((int) str1[i]);
			} else {
				str1[i] = (char) toupper((int) str1[i]);
			}
			i++;
		}

		write(down[1], str1, 255);	// sends altered string back to the sender
		close(down[1]);	// closes unnecessary descriptor

		wait(NULL);
	} else if(pid == 0) {
		close(down[1]);	// closes unnecessary descriptor
		close(up[0]);		// closes unnecessary descriptor

		char str2[255];

		printf("CLIENT: String to send to server\n");
		fgets(str2, 255, stdin);	// reads string from console

		printf("CLIENT: Message sent to Server: %s\n", str2);
		write(up[1], str2, 255);	// sends string to Server
		close(up[1]);	// closes unnecessary descriptor

		read(down[0], str2, 255);	// receives converter string from Server
		close(down[0]);	// closes unnecessary descriptor
		printf("CLIENT: String received from server: %s\n", str2);

		exit(0);
	} else if(pid == -1) {
		perror("Error");
	}

	return 0;
}
