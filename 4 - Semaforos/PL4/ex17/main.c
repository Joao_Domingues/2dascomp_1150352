#include <semaphore.h>
#include <stdio.h>
#include <wait.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

#define PROCESS_NUMBER 30
#define NORMAL 0
#define SPECIAL 1
#define VIP 2

int main() {
  int i, client_type, capacity_value;
  time_t t;
  srand ((unsigned) time (&t));

  sem_t * vip;
  sem_t * special;
  sem_t * normal;
  sem_t * capacity;

  if((vip = sem_open("name1", O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
    if((vip = sem_open("name1", O_EXCL, 0644, 0)) == SEM_FAILED) {
      perror("ERROR - sem_open failed");
      exit(1);
    }
  }

  if((special = sem_open("name2", O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
    if((special = sem_open("name2", O_EXCL, 0644, 0)) == SEM_FAILED) {
      perror("ERROR - sem_open failed");
      exit(1);
    }
  }

  if((normal = sem_open("name3", O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
    if((normal = sem_open("name3", O_EXCL, 0644, 0)) == SEM_FAILED) {
      perror("ERROR - sem_open failed");
      exit(1);
    }
  }

  if((capacity = sem_open("name4", O_CREAT|O_EXCL, 0644, 300)) == SEM_FAILED) {
    if((capacity = sem_open("name4", O_EXCL, 0644, 300)) == SEM_FAILED) {
      perror("ERROR - sem_open failed");
      exit(1);
    }
  }

  /*if(sem_unlink("name1") == -1 || sem_unlink("name2") == -1 || sem_unlink("name3") == -1 || sem_unlink("name4") == -1) {
    perror("ERROR - sem_unlink failed");
  }*/

  pid_t pid;
  for(i = 0; i < PROCESS_NUMBER; i++) {
    client_type = rand () % 3;
    pid = fork();
    if(pid == -1) {
      perror("ERROR - process creation failed");
      exit(1);
    } else if(pid == 0) {
      break;
    }
  }

  if(i == PROCESS_NUMBER) {

    for(i=0;i<PROCESS_NUMBER;i++) {
      wait(NULL);
    }
  } else {

    if(sem_trywait(capacity) == 0) {
      sem_getvalue(capacity, &capacity_value);
      if(client_type == NORMAL) {
        sem_post(normal);
        printf("PID %d, a normal client, entered the room... there are %d seats left...\n", getpid(), capacity_value);
      }
      if(client_type == SPECIAL) {
        sem_post(special);
        printf("PID %d, a special client, entered the room... there are %d seats left...\n", getpid(), capacity_value);
      }
      if(client_type == VIP) {
        sem_post(vip);
        printf("PID %d, a vip client, entered the room... there are %d seats left...\n", getpid(), capacity_value);
      }

      sleep(10);

      int num_vip, num_special, num_normal;
      if(client_type == NORMAL) {
        do{
          sem_getvalue(vip, &num_vip);
          sem_getvalue(special, &num_special);
        } while(num_vip > 0 || num_special > 0);
        sleep(1);
        sem_getvalue(normal, &num_normal);
        sem_wait(normal);
        printf("PID %d left the theater... there are still %d normal clients inside the theater.\n", getpid(), num_normal);
      }
      if(client_type == SPECIAL) {
        do{
          sem_getvalue(vip, &num_vip);
        } while(num_vip > 0);
        sleep(1);
        sem_getvalue(special, &num_special);
        sem_wait(special);
        printf("PID %d left the theater... there are still %d special clients inside the theater.\n", getpid(), num_special);
      }
      if(client_type == VIP) {
        sem_getvalue(vip, &num_vip);
        sem_wait(vip);
        printf("PID %d left the theater... there are still %d vip clients inside the theater.\n", getpid(), num_vip);
      }
      sem_post(capacity);
    } else {
      printf("PID %d - the room is full, this client is leaving...\n", getpid());
    }
    exit(0);
  }

  if(sem_unlink("name1") == -1 || sem_unlink("name2") == -1 || sem_unlink("name3") == -1) {
    perror("ERROR - sem_unlink failed");
  }
  puts("\nThe theater is closed... exiting");
  return 0;
}
