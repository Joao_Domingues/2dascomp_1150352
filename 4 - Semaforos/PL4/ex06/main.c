#include <semaphore.h>
#include <stdio.h>
#include <wait.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

int main() {

  int i;
  sem_t * semaphore[2];
  char names[2][5];
  strcpy(names[0], "name1");
  strcpy(names[1], "name2");

  for(i = 0; i < 2; i++) {
    if((semaphore[i] = sem_open(names[i], O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
      if((semaphore[i] = sem_open(names[i], O_EXCL, 0644, 0)) == SEM_FAILED) {
        perror("ERROR - sem_open failed");
        exit(1);
      }
    }
  }

  sem_post(semaphore[0]);
  switch(fork()) {
    case -1:
      perror("ERROR - process creation failed");
      exit(1);
    case 0:
      for(i=0;i<15;i++) {
        sem_wait(semaphore[0]);
        printf("I'm the child\n");
        sem_post(semaphore[1]);
      }
      exit(0);
    default:
      for(i=0;i<15;i++) {
        sem_wait(semaphore[1]);
        printf("I'm the father\n");
        sem_post(semaphore[0]);
      }
      break;
  }

  for(i=0;i<2;i++) {
    if(sem_unlink(names[i]) == -1) {
      perror("ERROR - sem_unlink failed");
    }
  }

  return 0;
}
