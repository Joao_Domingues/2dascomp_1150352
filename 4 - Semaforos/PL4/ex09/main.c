#include <semaphore.h>
#include <stdio.h>
#include <wait.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

int main() {
  int i, PROCESS_NUMBER = 2;
  sem_t * semaphores[PROCESS_NUMBER];
  char names[PROCESS_NUMBER][10];
  strcpy(names[0], "name1");
  strcpy(names[1], "name2");

  for(i = 0; i < PROCESS_NUMBER; i++) {
    if((semaphores[i] = sem_open(names[i], O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
      if((semaphores[i] = sem_open(names[i], O_EXCL, 0644, 0)) == SEM_FAILED) {
        perror("ERROR - sem_open failed");
        exit(1);
      }
    }
  }

  pid_t pid;
  for(i = 0; i < PROCESS_NUMBER - 1; i++) {
    pid = fork();
    if(pid == -1) {
      perror("ERROR - process creation failed");
      exit(1);
    } else if(pid == 0) {
      break;
    }
  }
  //sleep(1);
  int sem1, sem2;
  switch(i) {
    case 0:
        sem_post(semaphores[0]); // buy_chips();
        puts("Chips were bought");
        sem_getvalue(semaphores[0], &sem1);
        sem_getvalue(semaphores[1], &sem2);
        printf("Process 1: chips:%d beer:%d\n", sem1, sem2);
        if(sem1 == 1 && sem2 == 1) {
          puts("Process 1 ate and drank");
          sem_wait(semaphores[0]);
          sem_wait(semaphores[1]);
        }
      exit(0);
    case 1:
        sem_post(semaphores[1]); // buy_beer();
        puts("Beer was bought");
        sem_getvalue(semaphores[0], &sem1);
        sem_getvalue(semaphores[1], &sem2);
        printf("Process 2: chips:%d beer:%d\n", sem1, sem2);
        if(sem1 == 1 && sem2 == 1) {
          puts("Process 2 ate and drank");
          sem_wait(semaphores[0]);
          sem_wait(semaphores[1]);
        }
      break;
    default:
      break;
  }
  wait(NULL);
  for(i=0;i<PROCESS_NUMBER;i++) {
    if(sem_unlink(names[i]) == -1) {
      perror("ERROR - sem_unlink failed");
    }
  }

  return 0;
}
