#include <semaphore.h>
#include <stdio.h>
#include <wait.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

int main() {
  int i, PROCESS_NUMBER = 3;
  sem_t * semaphores[PROCESS_NUMBER];
  char names[PROCESS_NUMBER][10];
  strcpy(names[0], "name1");
  strcpy(names[1], "name2");
  strcpy(names[2], "name3");

  for(i = 0; i < PROCESS_NUMBER; i++) {
    if((semaphores[i] = sem_open(names[i], O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
      if((semaphores[i] = sem_open(names[i], O_EXCL, 0644, 0)) == SEM_FAILED) {
        perror("ERROR - sem_open failed");
        exit(1);
      }
    }
  }

  setbuf(stdout, NULL); // disables the stdout buffer for the messages to come out in the right order

  sem_post(semaphores[0]);
  pid_t pid;
  for(i = 0; i < PROCESS_NUMBER - 1; i++) {
    pid = fork();
    if(pid == -1) {
      perror("ERROR - process creation failed");
      exit(1);
    } else if(pid == 0) {
      break;
    }
  }

  switch(i) {
    case 0:
      sem_wait(semaphores[0]);
      printf("Sistemas ");
      sem_post(semaphores[1]);

      sem_wait(semaphores[0]);
      printf("a ");
      sem_post(semaphores[1]);
      exit(0);
    case 1:
      sem_wait(semaphores[1]);
      printf("de ");
      sem_post(semaphores[2]);

      sem_wait(semaphores[1]);
      printf("melhor ");
      sem_post(semaphores[2]);
      exit(0);
    case 2:
      sem_wait(semaphores[2]);
      printf("Computadores - ");
      sem_post(semaphores[0]);

      sem_wait(semaphores[2]);
      printf("disciplina!\n");
      break;
    default:
      break;
  }

  for(i=0;i<PROCESS_NUMBER;i++) {
    if(sem_unlink(names[i]) == -1) {
      perror("ERROR - sem_unlink failed");
    }
  }

  return 0;
}
