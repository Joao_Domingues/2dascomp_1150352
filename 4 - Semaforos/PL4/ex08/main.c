#include <semaphore.h>
#include <stdio.h>
#include <wait.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

int main() {
  int i, j = 0, PROCESS_NUMBER = 2;
  sem_t * semaphores[PROCESS_NUMBER];
  char names[PROCESS_NUMBER][10];
  strcpy(names[0], "name1");
  strcpy(names[1], "name2");

  for(i = 0; i < PROCESS_NUMBER; i++) {
    if((semaphores[i] = sem_open(names[i], O_CREAT|O_EXCL, 0644, 1)) == SEM_FAILED) {
      if((semaphores[i] = sem_open(names[i], O_EXCL, 0644, 1)) == SEM_FAILED) {
        perror("ERROR - sem_open failed");
        exit(1);
      }
    }
  }
  setbuf(stdout, NULL);
  pid_t pid;
  for(i = 0; i < PROCESS_NUMBER - 1; i++) {
    pid = fork();
    if(pid == -1) {
      perror("ERROR - process creation failed");
      exit(1);
    } else if(pid == 0) {
      break;
    }
  }

  switch(i) {
    case 0:
      while(1) {
        sem_wait(semaphores[0]);
        sleep(1);
        printf("S");
        sem_post(semaphores[1]);
        j++;
        if(j==100) break;
      }
      exit(0);
    case 1:
      while(1) {
        sem_wait(semaphores[1]);
        sleep(1);
        printf("C");
        sem_post(semaphores[0]);
        j++;
        if(j==100) break;
      }
      break;
    default:
      break;
  }

  for(i=0;i<PROCESS_NUMBER;i++) {
    if(sem_unlink(names[i]) == -1) {
      perror("ERROR - sem_unlink failed");
    }
  }

  return 0;
}
