#include <semaphore.h>
#include <stdio.h>
#include <wait.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <limits.h>


typedef struct {
  int number;
  char name[100];
  char address[100];
} user;

void presentUserInfo(user* u);

int main() {

  // Shared memory creation
  int fd = shm_open("/shmscomp", O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);
  if(fd == -1) {
    fd = shm_open("/shmscomp", O_RDWR, S_IRUSR | S_IWUSR);

    if(fd == -1) {
      perror("ERROR - shm_open failed");
      exit(0);
    }
  }

  int DATA_SIZE = sizeof(user);

  if(ftruncate(fd, DATA_SIZE) == -1) {
    perror("ERROR - ftruncate failed");
    exit(0);
  }

  user **users = mmap(NULL, 100 * DATA_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

  // Semaphores creation
  sem_t * canRead;

  if((canRead = sem_open("name1", O_CREAT|O_EXCL, 0644, 1)) == SEM_FAILED) {
    if((canRead = sem_open("name1", O_EXCL, 0644, 1)) == SEM_FAILED) {
      perror("ERROR - sem_open failed");
        exit(1);
    }
  }

  sem_t * canWrite;

  if((canWrite = sem_open("name2", O_CREAT|O_EXCL, 0644, 1)) == SEM_FAILED) {
    if((canWrite = sem_open("name2", O_EXCL, 0644, 1)) == SEM_FAILED) {
      perror("ERROR - sem_open failed");
        exit(1);
    }
  }

  int option, number, i, j, canReadValue, canWriteValue;
  char name[100], address[100];
  do{
    printf("\n --- Menu ---\n 0 - Exit\n 1 - Consult\n 2 - Consult All\n 3 - Insert\n");
    scanf("%d", &option);

    switch(option) {
      case 0:
        printf("Exiting\n");
        break;
      case 1:
        sem_getvalue(canRead, &canReadValue);
        if(canReadValue == 0) { // waits if a wait is needed, proceeds if a wait is not needed
          sem_wait(canRead);
        }

        printf("\nInsert the user's number:\n");
        scanf("%d", &number);
        puts("x");
        for(i = 0; i < 100; i++) {
          if(users[i]->number == number) {
            presentUserInfo(users[i]);
            break;
          }
        }

        sem_post(canRead);
        break;
      case 2:
        for(i = 0; i < 100; i++) {
          presentUserInfo(users[i]);
        }
        break;
      case 3:
        sem_wait(canRead);
        printf("\nNew user\n");
        do{
          j = 0;
          printf("Number:\n");
          scanf("%d", &number);
          for(i = 0; i < 100; i++) {
            if(users[i]->number == number) j = 1;
          }
        } while(j == 1);
        for(i = 0; i < 100; i++) {
          if(users[i]->number == 0);
          break;
        }
        if(i < 100) {
          users[i]->number = number;
          puts("x");
          printf("Name:\n");
          fgets(name, 100, stdout);
          strcpy(users[i]->name, name);
          printf("Address:\n");
          fgets(address, 100, stdout);
          strcpy(users[i]->address, address);
        }
        sem_post(canRead);
        break;
      default:
        printf("Please insert a valid option");
        break;
    }
  } while(option != 0);

  // close waiting
  if(sem_unlink("name1") == -1) {
    perror("ERROR - sem_unlink failed");
  }
  if(sem_unlink("name2") == -1) {
    perror("ERROR - sem_unlink failed");
  }

  // close shared memory
  if(munmap(users, DATA_SIZE) < 0) {
    perror("ERROR - munmap failed");
    exit(0);
  }
    if(shm_unlink("/shmscomp") < 0) {
    perror("ERROR - shm_unlink failed");
    exit(0);
  }

  return 0;
}

void presentUserInfo(user* u) {
  printf("\nUser\nNumber: %d\nName: %s\nAddress: %s\n", u->number, u->name, u->address);
}
