#include <semaphore.h>
#include <stdio.h>
#include <wait.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

int main() {

  sem_t * semaphore;
  if((semaphore = sem_open("sem_name", O_CREAT|O_EXCL, 0644, 1)) == SEM_FAILED) {
    semaphore = sem_open("sem_name", O_EXCL, 0644, 1);
  }

  FILE * file;
  int i, numbers[200];
  pid_t pid;

  for(i = 0; i < 8; i++) {
    pid = fork();
    if(pid == 0) {
      break;
    } else if(pid == -1) {
      perror("ERROR - process creation failed\n");
      exit(1);
    }
  }

  if(i < 8) {
    printf("Child index: %d - executing sem_wait\n", i);
    if(sem_wait(semaphore) == -1) perror("ERROR - sem_wait failed");
    printf("Child index: %d - executing code\n", i);

    //open, read from, close Numbers.txt
    int j = 0;
    file = fopen("Numbers.txt", "r");
    if(file) {
      while(!feof(file) && j < 200) {
        fscanf(file, "%d", &numbers[j]);
        j++;
      }
    } else {
      perror("ERROR - Can't open Numbers.txt file");
      exit(1);
    }
    fclose(file);

    //open, write to, close Output.txt
    j = 0;
    file = fopen("Output.txt", "a");
    if(file) {
      for(j = 0; j < 200; j++) {
        fprintf(file, "%d\n", numbers[j]);
      }
    } else {
      perror("ERROR - Can't open Output.txt file");
      exit(1);
    }
    fclose(file);

    printf("Child index: %d - executing sem_post\n", i);
    sem_post(semaphore);
    exit(0);
  }

  for(i = 0; i < 8; i++) {
    wait(NULL);
  }

  printf("\nOutput.txt\n");

  int counter = 0;
  file = fopen("Output.txt", "r");
  if(file) {
    while(!feof(file)) {
      counter++;
      fscanf(file, "%d", &i);
      //printf("%d\n", i);
    }
  } else {
    perror("ERROR - Can't open Numbers.txt file");
  }
  fclose(file);

  printf("Number of ints read from Output.txt: %d\n", counter);

  if(sem_unlink("sem_name") == -1) {
    perror("ERROR - sem_unlink failed");
  }

  return 0;
}
