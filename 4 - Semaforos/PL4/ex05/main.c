#include <semaphore.h>
#include <stdio.h>
#include <wait.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

int main() {

  sem_t * semaphore;
  if((semaphore = sem_open("sem_name", O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
    if((semaphore = sem_open("sem_name", O_EXCL, 0644, 0)) == SEM_FAILED) {
      perror("ERROR - sem_open failed");
      exit(1);
    }
  }

  int i;

  switch(fork()) {
    case -1:
      perror("ERROR - process creation failed");
      exit(1);
    case 0:
      printf("I'm the child\n");
      sem_post(semaphore);
      for(i=0;i<99;i++) printf("I'm the child %d\n", i);
      exit(0);
    default:
      sem_wait(semaphore);
      for(i=0;i<100;i++) printf("I'm the father %d\n", i);
      break;
  }

  if(sem_unlink("sem_name") == -1) {
    perror("ERROR - sem_unlink failed");
    exit(1);
  }

  return 0;
}
