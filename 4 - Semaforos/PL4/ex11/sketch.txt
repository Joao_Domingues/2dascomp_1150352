sem_t * sem1, sem2, sem3; // inicializados a 0
sem_t * sem4; // inicializado a 200

for(i=0->300) {
  if(fork() == 0) break;
}

switch(i) {
  case 300:
    // não faz nada
    break;

  default:

    down(sem4);
    while(1) {
      if(sem_getvalue(sem_x) == 1) { // one if for each door
        down(sem_x);
        // stay inside train for 10 seconds
        up(sem_x);
      }
    }
    up(sem4);
}
