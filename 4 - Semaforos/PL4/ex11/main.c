#include <semaphore.h>
#include <stdio.h>
#include <wait.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define SEM_NUMBER 4
sem_t * semaphores[SEM_NUMBER];

void goThroughDoor();

int main() {
  int i, sem_value, PROCESS_NUMBER = 300;
  char names[SEM_NUMBER][10];
  strcpy(names[0], "name1"); // door 1
  strcpy(names[1], "name2"); // door 2
  strcpy(names[2], "name3"); // door 3
  strcpy(names[3], "name4"); // train capacity

  for(i = 0; i < SEM_NUMBER-1; i++) {
    if((semaphores[i] = sem_open(names[i], O_CREAT|O_EXCL, 0644, 1)) == SEM_FAILED) {
      if((semaphores[i] = sem_open(names[i], O_EXCL, 0644, 1)) == SEM_FAILED) {
        perror("ERROR - sem_open failed");
        exit(1);
      }
    }
  }

  if((semaphores[SEM_NUMBER-1] = sem_open(names[SEM_NUMBER-1], O_CREAT|O_EXCL, 0644, 200)) == SEM_FAILED) {
    if((semaphores[SEM_NUMBER-1] = sem_open(names[SEM_NUMBER-1], O_EXCL, 0644, 200)) == SEM_FAILED) {
      perror("ERROR - sem_open failed");
      exit(1);
    }
  }

  pid_t pid;
  for(i = 0; i < PROCESS_NUMBER; i++) {
    pid = fork();
    if(pid == -1) {
      perror("ERROR - process creation failed");
      exit(1);
    } else if(pid == 0) {
      break;
    }
  }

  switch(i) {
    case 300:
      break;
    default:

      sem_getvalue(semaphores[3], &sem_value);
      printf("Current queue capacity: %d\n", sem_value);
      sem_wait(semaphores[3]); // check if the train is full

      goThroughDoor();  // tries to use first available door and takes 1 second going through it
      sem_getvalue(semaphores[3], &sem_value);
      printf("Process %d entered the train - current queue capacity: %d\n", getpid(), sem_value);
      sleep(10);        // stays inside the train 10 seconds
      goThroughDoor();

      sem_post(semaphores[3]); // leaving the train, opening a spot
      sem_getvalue(semaphores[3], &sem_value);
      printf("Process %d left the train - current queue capacity: %d\n", getpid(), sem_value);
      exit(0);
  }

  for(i=0;i<PROCESS_NUMBER;i++) {
    wait(NULL);
    printf("wait %d\n", i);
  }
  for(i=0;i<SEM_NUMBER;i++) {
    if(sem_unlink(names[i]) == -1) {
      perror("ERROR - sem_unlink failed");
    }
  }

  return 0;
}

void goThroughDoor() {
  int sem_value;
  while(1) {
    sem_getvalue(semaphores[0], &sem_value);
    if(sem_value == 1) {
      sem_wait(semaphores[0]);
      sleep(1);
      sem_post(semaphores[0]);
      break;
    }
    sem_getvalue(semaphores[1], &sem_value);
    if(sem_value == 1) {
      sem_wait(semaphores[1]);
      sleep(3);
      sem_post(semaphores[1]);
      break;
    }
    sem_getvalue(semaphores[2], &sem_value);
    if(sem_value == 1) {
      sem_wait(semaphores[2]);
      sleep(2);
      sem_post(semaphores[2]);
      break;
    }
  }
}
