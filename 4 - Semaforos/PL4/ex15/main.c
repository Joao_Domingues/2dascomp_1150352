#include <semaphore.h>
#include <stdio.h>
#include <wait.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

int main() {
  int i, PROCESS_NUMBER = 10;

  sem_t * door;
  sem_t * show;
  sem_t * showEnded;

  if((door = sem_open("name1", O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
    if((door = sem_open("name1", O_EXCL, 0644, 0)) == SEM_FAILED) {
      perror("ERROR - sem_open failed");
      exit(1);
    }
  }

  if((show = sem_open("name2", O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
    if((show = sem_open("name2", O_EXCL, 0644, 0)) == SEM_FAILED) {
      perror("ERROR - sem_open failed");
      exit(1);
    }
  }

  if((showEnded = sem_open("name3", O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
    if((showEnded = sem_open("name3", O_EXCL, 0644, 0)) == SEM_FAILED) {
      perror("ERROR - sem_open failed");
      exit(1);
    }
  }

  /*if(sem_unlink("name1") == -1 || sem_unlink("name2") == -1 || sem_unlink("name3") == -1) {
    perror("ERROR - sem_unlink failed");
  }*/

  pid_t pid;
  for(i = 0; i < PROCESS_NUMBER; i++) {
    pid = fork();
    if(pid == -1) {
      perror("ERROR - process creation failed");
      exit(1);
    } else if(pid == 0) {
      break;
    }
  }

  switch(i) {
    case 10:
      printf("The crowd is gathering around the show room... %d people showed up to the concerts!\nThe first concert will begin within a few seconds, there are only 5 spots to take!\n", PROCESS_NUMBER);

      sleep(5);
      puts("\nThe first concert is now starting, the doors are being open.");
      sem_post(show);
      for(i=0; i<5;i++) sem_post(door);
      sleep(20);
      sem_wait(show);
      puts("\nThe first concert just ended! Closing doors. Please stand by, the second show will begin shortly...");

      sleep(10);
      puts("\nThe second concert is now starting, the doors are being open.");
      sem_post(show);
      for(i=0; i<5;i++) sem_post(door);
      sleep(20);
      sem_wait(show);
      puts("\nThe second concert just ended! Closing doors. Please stand by, the third show will begin shortly...");

      sleep(10);
      puts("\nThe third concert is now starting, the doors are being open.");
      sem_post(show);
      for(i=0; i<5;i++) sem_post(door);
      sleep(20);
      sem_wait(show);
      puts("\nThe third concert just ended! Closing doors. Thank you everyone for showing up and see you soon!");
      for(i=0; i<=PROCESS_NUMBER;i++) sem_post(showEnded);

      for(i=0;i<PROCESS_NUMBER;i++) {
        wait(NULL);
      }
      break;
    default:;

      int showStatus;
      while(1) {

        sem_getvalue(show, &showStatus);
        if(sem_trywait(door) == 0 && showStatus == 1) { // se houver uma porta disponível
          printf("PID %d entered the room!\n", getpid());
          sleep(5);
          printf("PID %d left the room!\n", getpid());
          sem_post(door);
        }

        if(sem_trywait(showEnded) == 0) exit(0);
      }
  }

  if(sem_unlink("name1") == -1 || sem_unlink("name2") == -1 || sem_unlink("name3") == -1) {
    perror("ERROR - sem_unlink failed");
  }
  puts("exiting");
  return 0;
}
