#include <semaphore.h>
#include <stdio.h>
#include <wait.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

int main() {
  const int PROCESS_NUMBER = 10;
  int i, side;
  time_t t;
  srand ((unsigned) time (&t));

  sem_t * WE_dir;
  sem_t * EW_dir;
  sem_t * end;

  if((WE_dir = sem_open("name1", O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
    if((WE_dir = sem_open("name1", O_EXCL, 0644, 0)) == SEM_FAILED) {
      perror("ERROR - sem_open failed");
      exit(1);
    }
  }

  if((EW_dir = sem_open("name2", O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
    if((EW_dir = sem_open("name2", O_EXCL, 0644, 0)) == SEM_FAILED) {
      perror("ERROR - sem_open failed");
      exit(1);
    }
  }

  if((end = sem_open("name3", O_CREAT|O_EXCL, 0644, 1)) == SEM_FAILED) {
    if((end = sem_open("name3", O_EXCL, 0644, 1)) == SEM_FAILED) {
      perror("ERROR - sem_open failed");
      exit(1);
    }
  }

  /*if(sem_unlink("name1") == -1 || sem_unlink("name2") == -1 || sem_unlink("name3") == -1) {
    perror("ERROR - sem_unlink failed");
  }*/

  pid_t pid;
  for(i = 0; i < PROCESS_NUMBER; i++) {
    side = rand () % 2;
    pid = fork();
    if(pid == -1) {
      perror("ERROR - process creation failed");
      exit(1);
    } else if(pid == 0) {
      break;
    }
    sleep(rand() % 5 + 5);
  }

  if(i == PROCESS_NUMBER) {
    sleep(60);
    sem_wait(end);

    for(i=0;i<PROCESS_NUMBER;i++) {
      wait(NULL);
    }
  } else {
    int programStatus, bridgeStatus;
    if(side == 0) printf("O  PID %d arrived at the scene at the West side\n", getpid());
    if(side == 1) printf("O  PID %d arrived at the scene at the East side\n", getpid());

    while(1) {

      if(side == 0) {
        sem_getvalue(EW_dir, &bridgeStatus);
        if(bridgeStatus == 0) {
          printf("-> PID %d entered bridge, going from West to East\n", getpid());
          sem_post(WE_dir);
          sleep(10);
          sem_getvalue(WE_dir, &bridgeStatus);
          printf("PID %d reached the East side, there are still %d cars on the bridge\n", getpid(), bridgeStatus-1);
          sem_wait(WE_dir);
          side = 1;
        }
      } else if(side == 1) {
        sem_getvalue(WE_dir, &bridgeStatus);
        if(bridgeStatus == 0) {
          printf("<- PID %d entered bridge, going from East to West\n", getpid());
          sem_post(EW_dir);
          sleep(10);
          sem_getvalue(EW_dir, &bridgeStatus);
          printf("PID %d reached the West side, there are still %d cars on the bridge\n", getpid(), bridgeStatus-1);
          sem_wait(EW_dir);
          side = 0;
        }
      }
      sleep(rand() % 10 + 5);
      sem_getvalue(end, &programStatus);
      if(programStatus == 0) exit(0);
    }
  }

  if(sem_unlink("name1") == -1 || sem_unlink("name2") == -1 || sem_unlink("name3") == -1) {
    perror("ERROR - sem_unlink failed");
  }
  puts("\nThe bridge is closed... exiting");
  return 0;
}
