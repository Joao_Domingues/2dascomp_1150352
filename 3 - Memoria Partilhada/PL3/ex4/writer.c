#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct {
  int number;
} value;

int main() {

	int DATA_SIZE = sizeof(value);

	value * value_1;

	int fd = shm_open("/shmscomp", O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);
  if(fd == -1) {
    fd = shm_open("/shmscomp", O_RDWR, S_IRUSR | S_IWUSR);

    if(fd == -1) {
      perror("ERROR - shm_open failed");
      exit(0);
    }
  }

	if(ftruncate(fd, DATA_SIZE) == -1) {
    perror("ERROR - ftruncate failed");
    exit(0);
  }

  value_1 = (value*) mmap(NULL, DATA_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
/*  if(value_1 == -1) {
    perror("ERROR - mmap failed");
    exit(0);
  }
*/
  value_1->number = 100;
  int i;

  pid_t pid = fork();
  printf("pid%d\n", pid);
  for(i = 0; i < 1000000; i++) {
    if(pid == -1) {
      perror("ERROR - fork failed");
      exit(0);
    } else if(pid == 0) {
      value_1->number++;
      value_1->number--;
    } else {
      value_1->number--;
      value_1->number++;
    }
  }

  if(pid == 0) exit(0);

  printf("Final value: %d\n", value_1->number);

  if(munmap(value_1, DATA_SIZE) < 0) {
    perror("ERROR - munmap failed");
    exit(0);
  }

  if(shm_unlink("/shmscomp") < 0) {
    perror("ERROR - shm_unlink failed");
    exit(0);
  }

	return 0;
}
