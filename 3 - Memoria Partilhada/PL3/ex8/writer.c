#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define STR_SIZE 50
#define NR_DISC 10

typedef struct {
  int numero;
  char nome[STR_SIZE];
  int disciplinas[NR_DISC];
  int flag;
}aluno;

int main() {

  int DATA_SIZE = sizeof(aluno);
  aluno * aluno1;

  int fd = shm_open("/shmscomp", O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);
  if(fd == -1) {
    fd = shm_open("/shmscomp", O_RDWR, S_IRUSR | S_IWUSR);

    if(fd == -1) {
      perror("ERROR - shm_open failed");
      exit(0);
    }
  }

	if(ftruncate(fd, DATA_SIZE) == -1) {
    perror("ERROR - ftruncate failed");
    exit(0);
  }

  aluno1 = (aluno*) mmap(NULL, DATA_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

  switch(fork()) {
    case 0:
      while(aluno1->flag == 0);
      int max = 0, min = INT_MAX, total = 0, j;
      double average;

      for(j=0; j<NR_DISC; j++) {
        if(aluno1->disciplinas[j] < min) min = aluno1->disciplinas[j];
        if(aluno1->disciplinas[j] > max) max = aluno1->disciplinas[j];
        total += aluno1->disciplinas[j];
      }
      average = total / NR_DISC;

      printf("Student info:\n - Name: %s\n - Number: %d\n - Highest grade: %d\n - Lowest grade: %d\n - Average: %.2f\n", aluno1->nome, aluno1->numero, max, min, average);
      exit(0);
      break;
    case -1:
      perror("ERROR - process creation failed");
      exit(1);
    default:
      printf("Insert the student's number:\n");
      scanf("%d", &aluno1->numero);
      printf("Insert the student's name:\n");
      getchar();
      fgets(aluno1->nome, STR_SIZE, stdin);
      aluno1->nome[strlen(aluno1->nome)-1] = '\0';
      int i;
      printf("Insert the student's grades:\n");
      for(i=0; i<NR_DISC; i++) {
        scanf("%d", &aluno1->disciplinas[i]);
      }
      aluno1->flag = 1;
      break;
  }

  wait(NULL);

  if(munmap(aluno1, DATA_SIZE) < 0) {
    perror("ERROR - munmap failed");
    exit(0);
  }
    if(shm_unlink("/shmscomp") < 0) {
    perror("ERROR - shm_unlink failed");
    exit(0);
  }

	return 0;
}
