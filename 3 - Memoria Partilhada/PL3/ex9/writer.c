#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define STR_SIZE 20
#define FILENAME_SIZE 255

typedef struct {
  char [FILENAME_SIZE][10];
  char word[STR_SIZE];
  int number;
  int flag[10];
} process;

int main() {

  int DATA_SIZE = sizeof(process1);
  process * process1;

  int fd = shm_open("/shmscomp", O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);
  if(fd == -1) {
    fd = shm_open("/shmscomp", O_RDWR, S_IRUSR | S_IWUSR);

    if(fd == -1) {
      perror("ERROR - shm_open failed");
      exit(0);
    }
  }

	if(ftruncate(fd, DATA_SIZE) == -1) {
    perror("ERROR - ftruncate failed");
    exit(0);
  }

  process1 = (process*) mmap(NULL, DATA_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

  int i;
  pid_t pid;
  for(i=0; i<10; i++) {
    pid = fork();
    if(pid == 0) {
      break;
    } else if(pid == -1) {
      perror("ERROR - process creation failed");
    }
  }

  switch(fork()) {
    case 0:

      break;
    case -1:
      perror("ERROR - process creation failed");
      exit(1);
    default:

      break;
  }

  wait(NULL);

  if(munmap((process1), DATA_SIZE) < 0) {
    perror("ERROR - munmap failed");
    exit(0);
  }
    if(shm_unlink("/shmscomp") < 0) {
    perror("ERROR - shm_unlink failed");
    exit(0);
  }

	return 0;
}
