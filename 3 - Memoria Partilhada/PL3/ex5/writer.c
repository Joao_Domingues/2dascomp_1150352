#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct {
  int numbers[10];
} maxValues;

int main() {

	int DATA_SIZE = sizeof(maxValues);

	maxValues * maxValues_1;

	int fd = shm_open("/shmscomp", O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);
  if(fd == -1) {
    fd = shm_open("/shmscomp", O_RDWR, S_IRUSR | S_IWUSR);

    if(fd == -1) {
      perror("ERROR - shm_open failed");
      exit(0);
    }
  }

	if(ftruncate(fd, DATA_SIZE) == -1) {
    perror("ERROR - ftruncate failed");
    exit(0);
  }

  maxValues_1 = (maxValues*) mmap(NULL, DATA_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
/*  if(maxValues_1 == -1) {
    perror("ERROR - mmap failed");
    exit(0);
  }
*/

  int numbers[1000];
  int i;
  time_t t;
  srand ((unsigned) time (&t));
  for(i = 0; i < 1000; i++) numbers[i] = rand() % 1001;

  pid_t pid;
  for(i = 0; i < 10; i++) {
    pid = fork();
    if(pid == -1) {
      perror("ERROR - process creation failed");
    } else if(pid == 0) {
      break;
    }
  }

  if(i < 10) {
    int max = 0, j;
    for(j = 100*i; j < (i+1)*100; j++) {
      if(numbers[j] > max) {
        max = numbers[j];
      }
    }
    maxValues_1->numbers[i] = max;
    exit(0);
  } else {
    for(i = 0; i < 10; i++) {
      wait(NULL);
    }

    int max = 0;
    for(i = 0; i < 10; i++) {
      if(maxValues_1->numbers[i] > max) {
        max = maxValues_1->numbers[i];
      }
    }

    printf("Max value in array: %d\n", max);
  }

  if(munmap(maxValues_1, DATA_SIZE) < 0) {
    perror("ERROR - munmap failed");
    exit(0);
  }

  if(shm_unlink("/shmscomp") < 0) {
    perror("ERROR - shm_unlink failed");
    exit(0);
  }

	return 0;
}
