#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct {
  int numbers[10];
} array;

int main() {

	int DATA_SIZE = sizeof(array);

	array * array_1;

	int fd = shm_open("/shmscomp", O_RDWR, S_IRUSR | S_IWUSR);
	if(fd == -1) {
		perror("ERROR - shm_open failed");
		exit(0);
	}

	if(ftruncate(fd, DATA_SIZE) == -1) {
		perror("ERROR - ftruncate failed");
		exit(0);
	}

	array_1 = (array*) mmap(NULL, DATA_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	/*if(array_1 == -1) {
		perror("ERROR - mmap failed");
		exit(0);
	}*/

	int i, total = 0;

  for(i = 0; i < 10; i++) total += array_1->numbers[i];

  printf("Average of the values in the array: %.1f\n", (double)total/10);

	if(munmap(array_1, DATA_SIZE) < 0) {
    perror("ERROR - munmap failed");
    exit(0);
  }

  if(shm_unlink("/shmscomp") < 0) {
    perror("ERROR - shm_unlink failed");
    exit(0);
  }

	return 0;
}
