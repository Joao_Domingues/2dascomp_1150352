#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct {
  int numbers[10];
} array;

int main() {

	int DATA_SIZE = sizeof(array);

	array * array_1;

	int fd = shm_open("/shmscomp", O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);
  if(fd == -1) {
    fd = shm_open("/shmscomp", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);

    if(fd == -1) {
      perror("ERROR - shm_open failed");
      exit(0);
    }
  }

	if(ftruncate(fd, DATA_SIZE) == -1) {
    perror("ERROR - ftruncate failed");
    exit(0);
  }

	array_1 = (array*) mmap(NULL, DATA_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
/*  if(array_1 == -1) {
    perror("ERROR - mmap failed");
    exit(0);
  }
*/

  int i;

  time_t t;
  srand ((unsigned) time (&t));
  for(i = 0; i < 10; i++) array_1->numbers[i] = rand() % 20 + 1;

	return 0;
}
