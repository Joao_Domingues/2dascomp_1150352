#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct {
  char name[255];
  int num;
  int flag;
} student;

int main() {

	int DATA_SIZE = sizeof(student);

	student * student_1;

	int fd = shm_open("/shmscomp", O_RDWR, S_IRUSR | S_IWUSR);
	if(fd == -1) {
		perror("ERROR - shm_open failed");
		exit(0);
	}

	if(ftruncate(fd, DATA_SIZE) == -1) {
		perror("ERROR - ftruncate failed");
		exit(0);
	}

	student_1 = (student*) mmap(NULL, DATA_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	/*if(student_1 == -1) {
		perror("ERROR - mmap failed");
		exit(0);
	}*/

	while(student_1->flag == 0);

	printf("\nReader:\n");
	printf("Student's name: %s", student_1->name);
	printf("Student's Number: %d\n", student_1->num);

	if(munmap(student_1, DATA_SIZE) < 0) {
    perror("ERROR - munmap failed");
    exit(0);
  }

  if(shm_unlink("/shmscomp") < 0) {
    perror("ERROR - shm_unlink failed");
    exit(0);
  }

	return 0;
}
