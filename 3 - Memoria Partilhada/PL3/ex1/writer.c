#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct {
  char name[255];
  int num;
  int flag;
} student;

int main() {

	int DATA_SIZE = sizeof(student);

	student * student_1;

	int fd = shm_open("/shmscomp", O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);
  if(fd == -1) {
    fd = shm_open("/shmscomp", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);

    if(fd == -1) {
      perror("ERROR - shm_open failed");
      exit(0);
    }
  }

	if(ftruncate(fd, DATA_SIZE) == -1) {
    perror("ERROR - ftruncate failed");
    exit(0);
  }

	student_1 = (student*) mmap(NULL, DATA_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
/*  if(student_1 == -1) {
    perror("ERROR - mmap failed");
    exit(0);
  }
*/
  char string[255];
  printf("Student's name:\n");
  fgets(string, 255, stdin);
  strcpy(student_1->name, string);
  printf("Student's number:\n");
  scanf("%d", &(student_1->num));
  student_1->flag = 1;
  printf("Flag change from 0 to 1\n");

	return 0;
}
