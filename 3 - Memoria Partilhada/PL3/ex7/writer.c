#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct {
  int index;
  int number;
} data;

int main() {

	int DATA_SIZE = sizeof(data);
	data data_1;

  // random number section
  int numbers[1000];
  int i;
  time_t t;
  srand ((unsigned) time (&t));
  for(i = 0; i < 1000; i++) numbers[i] = rand() % 1001;

  int p[2];
  if(pipe(p) == -1) perror("ERROR - pipe creation failed");

  // fork section
  pid_t pid;
  for(i = 0; i < 10; i++) {
    pid = fork();
    if(pid == -1) {
      perror("ERROR - process creation failed");
    } else if(pid == 0) {
      break;
    }
  }

  if(i < 10) {
    close(p[0]);
    int max = 0, j;
    for(j = 100*i; j < (i+1)*100; j++) {
      if(numbers[j] > max) {
        max = numbers[j];
      }
    }
    data_1.index = i;
    data_1.number = max;

    write(p[1], &data_1, DATA_SIZE);
    close(p[1]);
    exit(0);
  } else {
    close(p[1]);
    int numbers[10];

    for(i = 0; i < 10; i++) {
      read(p[0], &data_1, DATA_SIZE);
      printf("i: %d | max value: %d\n", data_1.index, data_1.number);
      numbers[data_1.index] = data_1.number;
    }
    close(p[0]);

    int max = 0;
    for(i = 0; i < 10; i++) {
      if(numbers[i] > max) {
        max = numbers[i];
      }
    }

    printf("Max value in array: %d\n", max);
  }

	return 0;
}
