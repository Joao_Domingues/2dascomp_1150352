#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct {
  int numbers[10000000];
  int flag;
} test;

int main() {

	int DATA_SIZE = sizeof(test), i, ARRAY_SIZE = 10000000;
	test * test_1;
  int toSend[ARRAY_SIZE];

  // test creation
  time_t t;
  srand ((unsigned) time (&t));
  for(i = 0; i < ARRAY_SIZE; i++) toSend[i] = rand() % 100 + 1;

  pid_t pid = fork();
  if(pid == -1) {
    perror("ERROR - process creation failed");
    exit(1);
  }

  // timer start
  clock_t start = clock(), end, difference;

  // shared memory code
	int fd = shm_open("/shmscomp", O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);
  if(fd == -1) {
    fd = shm_open("/shmscomp", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);

    if(fd == -1) {
      perror("ERROR - shm_open failed");
      exit(0);
    }
  }
	if(ftruncate(fd, DATA_SIZE) == -1) {
    perror("ERROR - ftruncate failed");
    exit(0);
  }
	test_1 = (test*) mmap(NULL, DATA_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
/*  if(test_1 == -1) {
    perror("ERROR - mmap failed");
    exit(0);
  }
*/
  test_1->flag = 0;

  switch(pid) {
    case 0:
      for(i = 0; i < ARRAY_SIZE; i++) {
        test_1->numbers[i] = toSend[i];
      }
      test_1->flag = 1;
      exit(0);
      break;

    default:
      /* Nota para o professor:
        A meu ver, este ciclo while não terá influência no tempo total, desde que o número de cores
        a ser usado seja superior a 1, pois este ciclo estará a correr num dos cores enquanto que o
        processo filho corre no outro, não entrando em conflito.
      */
      while(test_1->flag == 0);
      for(i = 0; i < ARRAY_SIZE; i++) {
        toSend[i] = test_1->numbers[i];
      }
      end = clock();
      break;
  }

  difference = end - start;

  printf("\nTotal time spent transfering an array of %d integers from one process to another through shared memory using a minimum of 2 cores: %.2f ms\n", ARRAY_SIZE, 1000*((double)difference/CLOCKS_PER_SEC));

  if(munmap(test_1, DATA_SIZE) < 0) {
    perror("ERROR - munmap failed");
    exit(0);
  }

  if(shm_unlink("/shmscomp") < 0) {
    perror("ERROR - shm_unlink failed");
    exit(0);
  }

	return 0;
}
