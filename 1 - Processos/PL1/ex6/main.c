#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>

// alínea c
/*
int main() {

	int i, status;

	pid_t pid[4];
	
	for(i = 0; i < 4; i++) {
		pid[i] = fork();
		if(pid[i] == 0) {
			exit(0);
		} else if(pid[i] == -1) {
			perror("Error");
		}	
	}
	
	if(pid[0] > 0) {
		for(i = 0; i < 4; i++) {
			if(pid[i] % 2 == 0) {
				waitpid(pid[i], &status, 0);
				printf("This is the end of a process with even pid: %d.\n", pid[i]);
			}	
		}
	}

	return 0;
}
*/
// alínea d

int main() {

	int i, status;

	pid_t pid[4];
	
	for(i = 0; i < 4; i++) {
		pid[i] = fork();
		if(pid[i] == 0) {
			exit(i);
		}	
	}
	
	if(pid[0] > 0) {
		for(i = 0; i < 4; i++) {
			waitpid(pid[i], &status, 0);
			printf("Creation number of process %d: %d\n", pid[i], WEXITSTATUS(status));
		}
	}

	return 0;
}
