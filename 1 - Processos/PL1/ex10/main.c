#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

#define ARRAY_SIZE 2000

int main() {
	int numbers[2000];		/* array to lookup */
	int n;			/* the number to find */
	time_t t;			/* needed to initialize random number generator (RNG) */
	int i, j, total = 0, status;
	pid_t pid[10];

	/* intializes RNG (srand():stdlib.h; time(): time.h) */
	srand ((unsigned) time (&t));

	/* initialize array with random numbers (rand(): stdlib.h) */
	for (i = 0; i < ARRAY_SIZE; i++)
	  numbers[i] = rand () % 100;

	/* initialize n */
	n = rand () % 100;

	// --------------

	for(i = 0; i < 10; i++) {
		pid[i] = fork();
		if(pid[i] == 0) {
			for(j = 200 * i; j < 200 * (i+1); j++) {
				if(numbers[j] == n) exit(total);
				total++;
			}
			exit(255);
		} else if(pid[i] == -1) {
			perror("Error");
		}
	}

	for(j = 0; j < 10; j++) {
		waitpid(pid[j], &status, 0);
		printf("Creation index: %d - Position number %d was found: %d\n", j, n, WEXITSTATUS(status));
	}

	return 0;	
}
