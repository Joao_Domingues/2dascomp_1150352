#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

void filho(int *numbers, int i) {
	int j, max = 0;	
	
	for(j = i * 200; j < 200 * (i + 1); j++) {
		if(numbers[j] > max) max = numbers[j];
	}

	exit(max);
}

int main() {
	int numbers[1000];
	int result[1000];			
	time_t t;	
	int i, status, maximum = 0, p;

	srand ((unsigned) time (&t));

	for (i = 0; i < 1000; i++)
	  numbers[i] = rand () % 255;

	// --------------

	for(i = 0; i < 5; i++) {
		p=fork();
		if(p == 0) {
			filho(numbers, i);
		} else if(p > 0) {
			wait(&status);
			if(WEXITSTATUS(status) > maximum) maximum = WEXITSTATUS(status);
		} else if(p == -1) {
			perror("Error");
		}		
	}

	p=fork();
	if(p == 0) {
		for(i = 0; i < 500; i++) {
			result[i] = ((int) numbers[i]/maximum)*100;
			printf("index %d, value %d\n", i, result[i]);
		}
		exit(0);
	} else if(p > 0) {
		wait(&status);
		for(i = 500; i < 1000; i++) {
			result[i] = ((int) numbers[i]/maximum)*100;
			printf("index %d, value %d\n", i, result[i]);
		}
	} else if(p==-1) {
		perror("Error");
	}

	
	return 0;	
}

