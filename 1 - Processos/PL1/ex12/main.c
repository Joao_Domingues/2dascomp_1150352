#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

int spawn_child(int n) {
	int i, p;	
	for(i = 0; i < n; i++) {
		p=fork();
		if(p==0) break;
		if(p==-1) perror("Error");
	}
	if(i == n) return n-i;
	return (n-i) * 2;
}

int main() {
	int i, n = 6, status, value;
	if((value=spawn_child(n)) > 0) {
		printf("Return value: %d\n", value);
		exit(0);
	}
	for(i = 0; i < n; i++) wait(&status);
	printf("Return value: %d\n", value);
	return 0;	
}

