#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

void my_exec(char *command) {
	execlp(command, command, "-l", NULL);
}

int main() {
	char string[3][4];
	int p, status, i;
	
	// who, ls & ps
	scanf("%s", string[0]);
	scanf("%s", string[1]);
	scanf("%s", string[2]);

	for(i = 0; i<3; i++) {
		p=fork();
		if(p==0) {
			my_exec(string[i]);
		} else if(p==-1) {
			perror("Error");
		}
	}

	for(i=0;i<3;i++) {
		wait(&status);
		printf("Found: %d\n", WEXITSTATUS(status));
	}

	return 0;	
}

