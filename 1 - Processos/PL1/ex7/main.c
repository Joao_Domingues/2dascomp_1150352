#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

#define ARRAY_SIZE 1000

int main() {

	int numbers[ARRAY_SIZE];
	int n;
	time_t t;
	int i;

	srand ((unsigned) time(&t));

	for(i = 0; i < ARRAY_SIZE; i++)
		numbers[i] = rand () % 100;

	n = rand () % 100;

	pid_t pid = fork();
	int total = 0;
	int status;

	if(pid == 0) {
		for(i = ARRAY_SIZE/2; i < ARRAY_SIZE; i++) {	
			if(numbers[i] == n) {
				total++;
			}
		}
		exit(total);
	} else if(pid > 0) {
		for(i = 0; i < ARRAY_SIZE/2; i++) {	
			if(numbers[i] == n) {
				total++;
			}
		}
	} else if(pid == -1) {
		perror("Error");
	}

	wait(&status);
	total += WEXITSTATUS(status);

	printf("\n Número total de ocorrências do valor %d: %d\n", n, total);
	return 0;
}
