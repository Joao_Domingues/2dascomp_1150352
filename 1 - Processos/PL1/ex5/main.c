#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>

int main() {

	pid_t pid1, pid2;
	int status;
	printf("%d\n", getpid());
	pid1 = fork();

	if(pid1 > 0) {
		pid2 = fork();
	}
	
	if(pid1 == 0) {
		printf("%d\n", getppid());
		sleep(1);
		exit(1);
	}
	
	if(pid2 == 0) {
		printf("%d\n", getppid());
		sleep(2);
		exit(2);
	}	
	
	if(pid1 == -1 || pid2 == -1) {
		perror("Error");
	}	
	
	waitpid(pid1, &status, 0);
	printf("child process %d exit status: %d\n", pid1, WEXITSTATUS(status));
	waitpid(pid2, &status, 0);
	printf("child process %d exit status: %d\n", pid2, WEXITSTATUS(status));

	return 0;
}