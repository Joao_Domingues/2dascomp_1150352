#include <stdio.h>

int main() {

	int i, j, p;

	for(i = 0; i < 10; i++) {
		if(p=fork() == 0) break;
		if(p==-1) perror("Error");
	}

	for(j = 1; j <= 100; j++) {
		printf("Creation index: %d - Number: %d\n", 10-i, 100 * (9-i) + j);
	}
	
	return 0;
}

/*
	O output não está ordenado e não há garantia de estar sempre.
	Isto é devido ao escalonamento dos processos não ser certo na ordem
	de prioridade que dá a cada um na execução.
*/