#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

int main() {
	char string[30];
	int p, status;
	
	scanf("%s", string);
	
	while(string[0] != 0 && string[0] != 32) {
		p=fork();
		if(p==0) {
			execlp("ls", "ls", string, NULL);
		} else if(p==-1) {
			perror("Error");
		}
		wait(&status);
		printf("Found: %d\n", WEXITSTATUS(status));
		scanf("%s", string);
	}

	return 0;	
}
