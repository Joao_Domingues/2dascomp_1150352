#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>

int main() {
	
	pid_t pid1 = fork();
	pid_t pid2;
	pid_t pid3;

	if (pid1 > 0) {
		printf("I'm..\n");
		pid2 = fork();

		if (pid2 > 0) {
			printf("the..\n");
			pid3 = fork();
			
			if (pid3 > 0) {
				printf("father!\n");
			} else if (pid3 == -1) {
				perror("Error");
			} else {
				printf("I'll never join you!\n");
			}
		} else if (pid2 == -1) {
			perror("Error");
		} else {
			printf("I'll never join you!\n");
		}
	} else if (pid1 == -1) {
		perror("Error");
	} else {
		printf("I'll never join you!\n");
	}

	return 0;
}