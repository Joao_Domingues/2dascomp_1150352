#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

typedef struct {
  int numbers[5];
} key;

key keys[10000];
int result[49];

void * statisticalAnalysis(void * args);
void fillArray();

int main() {

  int i;
  pthread_t t[10];

  fillArray();
  for(i = 0; i < 10; i++) {
    pthread_create(&t[i], NULL, statisticalAnalysis, &i);
    pthread_join(t[i], NULL);
  }

  for(i = 0; i < 10; i++) {
  }

  for(i = 0; i < 49; i++) {
    printf("Number %d appears %d times\n", i+1, result[i]);
  }

  return 0;
}

void fillArray() {
  time_t t;
  srand ((unsigned) time (&t));
  int i, j, h, value, exists;

  for(i = 0; i < 10000; i++) {  // percorre vetor keys
    for(j = 0; j < 5; j++) {    // percorre cada key
      do {
        exists = 0;
        value = rand() % 49 + 1;
        for(h = 0; h < j; h++) {  // procura valores duplicados
          if(keys[i].numbers[j] == value) {
            exists = 1;
            break;
          }
        }
      } while (exists == 1);
      keys[i].numbers[j] = value;
    }
  }

  for(i = 0; i < 49; i++){
		result[i] = 0;
	}
}

void * statisticalAnalysis(void * args) {
  int i, j;

  for(i = 1000 * *(int *) args; i < 1000 * (*(int *) args + 1); i++) {
    for(j = 0; j < 5; j++) {
      result[keys[i].numbers[j] - 1]++;
    }
  }
  pthread_exit(NULL);
}
