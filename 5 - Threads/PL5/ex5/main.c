#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <limits.h>

int highestBalance, lowestBalance;
double average;
int balances[1000];

void * calculateHighestBalance(void * args);
void * calculateLowestBalance(void * args);
void * calculateAverage(void * args);
void fillArray();

int main() {

  fillArray();

  pthread_t t1, t2, t3;
  pthread_create(&t1, NULL, calculateHighestBalance, NULL);
  pthread_create(&t2, NULL, calculateLowestBalance, NULL);
  pthread_create(&t3, NULL, calculateAverage, NULL);

  pthread_join(t1, NULL);
  pthread_join(t2, NULL);
  pthread_join(t3, NULL);

  printf("Highest balance found: %d\nLowest balance found: %d\nAverage: %.2f\n", highestBalance, lowestBalance, average);
  return 0;
}

void fillArray() {
  time_t t;
  srand ((unsigned) time (&t));
  int i;

  for(i = 0; i < 1000; i++) {
    balances[i] = rand() % 50000;
  }
}

void * calculateHighestBalance(void * args) {
  int i, maxFound = 0;
  for(i = 0; i < 1000; i++) {
    if(balances[i] > maxFound) maxFound = balances[i];
  }
  highestBalance = maxFound;
  pthread_exit(NULL);
}

void * calculateLowestBalance(void * args) {
  int i, minFound = INT_MAX;
  for(i = 0; i < 1000; i++) {
    if(balances[i] < minFound) minFound = balances[i];
  }
  lowestBalance =  minFound;
  pthread_exit(NULL);
}

void * calculateAverage(void * args) {
  int i, total = 0;
  for(i = 0; i < 1000; i++) {
    total = total + balances[i];
  }

  average = total/1000;
  pthread_exit(NULL);
}
