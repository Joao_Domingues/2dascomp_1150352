typedef struct {
  int number;
  int notaG1;
  int notaG2;
  int notaG3;
  int notaFinal;
} Prova;

Prova * provas[300];

int pos;
int neg;
int currentIndex;

pthread_t t1;
pthread_t t2;
pthread_t t3;
pthread_t t4;
pthread_t t5;

pthread_mutex_t incP; // mutex for incrementPos
pthread_mutex_t incN; // mutex for incrementNeg
pthread_mutex_t calc; // mutex for calculate


generateProvas(); // T1
calculate(); // T2 & T3
incrementPos();   // T4
incrementNeg();   // T5

int main() {
  // init mutexes
  // lock mutexes
  // create threads on corresponding functions
  // join threads
}

generateProvas() {
  for(currentIndex=0->300) {
    // fill array provas with a new Prova, verifying if there are no duplicated prova->number's
    unlock(calc);
  }
}

calculate() {
  while(currentIndex < 300) {
    lock(calc);
    if(provas[currentIndex] >= 50) unlock(incP);
    if(provas[currentIndex] < 50) unlock(incN);
  }
}

incrementPos() {
  while(currentIndex < 300) {
    lock(incP);
    pos++;
  }
}

incrementNeg() {
  while(currentIndex < 300) {
    lock(incN);
    neg++;
  }
}
