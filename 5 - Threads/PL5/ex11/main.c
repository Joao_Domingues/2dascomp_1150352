#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

#define NUMBER_PROVAS 300

typedef struct {
  int number;
  int notaG1;
  int notaG2;
  int notaG3;
  int notaFinal;
} Prova;

Prova * provas[NUMBER_PROVAS];

int pos;
int neg;
int currentIndex;

pthread_t t1;
pthread_t t2;
pthread_t t3;
pthread_t t4;
pthread_t t5;

pthread_mutex_t incP; // mutex for incrementPos
pthread_mutex_t incN; // mutex for incrementNeg
pthread_mutex_t calc; // mutex for calculate


void * generateProvas(void * args); // T1
void * calculate(void * args); // T2 & T3
void * incrementPos(void * args);   // T4
void * incrementNeg(void * args);   // T5
int studentAlreadyExists(int number);

int main() {
  pos = 0;
  neg = 0;
  pthread_mutex_init(&incP, NULL);
	pthread_mutex_init(&incN, NULL);
	pthread_mutex_init(&calc, NULL);
  pthread_mutex_lock(&incP);
  pthread_mutex_lock(&incN);
  pthread_mutex_lock(&calc);

  pthread_create(&t1, NULL, generateProvas, NULL);
  pthread_create(&t2, NULL, calculate, NULL);
  pthread_create(&t3, NULL, calculate, NULL);
  pthread_create(&t4, NULL, incrementPos, NULL);
  pthread_create(&t5, NULL, incrementNeg, NULL);

  pthread_join(t1, NULL);
  pthread_join(t2, NULL);
  pthread_join(t3, NULL);
  pthread_join(t4, NULL);
  pthread_join(t5, NULL);

  printf("Positive percentage: %d\nNegative percentage: %d\n", pos/NUMBER_PROVAS, neg/NUMBER_PROVAS);

  return 0;
}

void * generateProvas(void * args) {
  int number, g1, g2, g3, notaFinal;
  time_t t;
  srand ((unsigned) time (&t));

  for(currentIndex = 0; currentIndex < NUMBER_PROVAS; currentIndex++) {

    Prova prova;
    g1 = rand() % 101;
    g2 = rand() % 101;
    g3 = rand() % 101;
    notaFinal = (g1+g2+g3)/3;

    do {
      number = rand() % NUMBER_PROVAS + 1160000;
    } while(studentAlreadyExists(number) == 1);

    prova.number = number;
    prova.notaG1 = g1;
    prova.notaG2 = g2;
    prova.notaG3 = g3;
    prova.notaFinal = notaFinal;
    provas[currentIndex] = &prova;

    pthread_mutex_unlock(&calc);
  }
  pthread_exit(NULL);
}

void * calculate(void * args) {
  int i;
  while(currentIndex < NUMBER_PROVAS) {
    i = currentIndex;
    pthread_mutex_lock(&calc);
    if(currentIndex == NUMBER_PROVAS) break;
    printf("CI:%d\n", currentIndex);
    if(provas[i]->notaFinal >= 50) {
      pthread_mutex_unlock(&incP);
    } else {
      pthread_mutex_unlock(&incN);
    }
  }

  pthread_exit(NULL);
}

void * incrementPos(void * args) {
  while(currentIndex < NUMBER_PROVAS) {
    pthread_mutex_lock(&incP);
    if(currentIndex == NUMBER_PROVAS) break;
    pos++;
  }
  pthread_exit(NULL);
}
void * incrementNeg(void * args) {
  while(currentIndex < NUMBER_PROVAS) {
    pthread_mutex_lock(&incN);
    if(currentIndex == NUMBER_PROVAS) break;
    neg--;
  }
  pthread_exit(NULL);
}

int studentAlreadyExists(int number) {
  int i;
  for (i = 0; i < currentIndex; i++) {
    if(provas[i]->number == number) return 1;
  }
  return 0;
}
