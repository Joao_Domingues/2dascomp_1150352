#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

#define CITY_A 1
#define CITY_B 2
#define CITY_C 3
#define CITY_D 4

pthread_mutex_t mAB;
pthread_mutex_t mBC;
pthread_mutex_t mBD;

void * travel(void * args);
void timeBurn();

int main() {

	pthread_mutex_init(&mAB, NULL);
	pthread_mutex_init(&mBC, NULL);
	pthread_mutex_init(&mBD, NULL);

  int i;

  pthread_t thread_id[5];
  for(i = 0; i < 5; i++) {
    pthread_create(&thread_id[i], NULL, travel, NULL);
  }
  for(i = 0; i < 5; i++) {
    pthread_join(thread_id[i], NULL);
  }

  return 0;
}

void * travel(void * args) {
  time_t t;
  srand ((unsigned) time (&t));

  int currentCity = rand() % 4 + 1;
  printf("Arrived at %d\n", currentCity);
  int nextCity;

  int i;
  for(i = 0; i < 4; i++) {
    if(currentCity == CITY_A) {
      pthread_mutex_lock(&mAB);
      printf("Train %d is traveling from City A to City B.\n", (int) pthread_self());
      timeBurn();
      printf("Train %d is arrived at City B, the trip took 5 seconds.\n", (int) pthread_self());
      pthread_mutex_unlock(&mAB);
      currentCity = CITY_B;
    } else if(currentCity == CITY_B) {

      do {
        nextCity = rand() % 4 + 1;
      } while (nextCity == CITY_B);

      if(nextCity == CITY_A) {
        pthread_mutex_lock(&mAB);
        printf("Train %d is traveling from City B to City A.\n", (int) pthread_self());
        sleep(5);
        printf("Train %d is arrived at City A, the trip took 5 seconds.\n", (int) pthread_self());
        pthread_mutex_unlock(&mAB);
        currentCity = CITY_A;
      } else if(nextCity == CITY_C) {
        pthread_mutex_lock(&mBC);
        printf("Train %d is traveling from City B to City C.\n", (int) pthread_self());
        sleep(5);
        printf("Train %d is arrived at City C, the trip took 5 seconds.\n", (int) pthread_self());
        pthread_mutex_unlock(&mBC);
        currentCity = CITY_C;
      } else if(nextCity == CITY_D) {
        pthread_mutex_lock(&mBD);
        printf("Train %d is traveling from City B to City D.\n", (int) pthread_self());
        sleep(5);
        printf("Train %d is arrived at City D, the trip took 5 seconds.\n", (int) pthread_self());
        pthread_mutex_unlock(&mBD);
        currentCity = CITY_D;
      }
    } else if(currentCity == CITY_C) {
      pthread_mutex_lock(&mBC);
      printf("Train %d is traveling from City C to City B.\n", (int) pthread_self());
      sleep(5);
      printf("Train %d is arrived at City B, the trip took 5 seconds.\n", (int) pthread_self());
      pthread_mutex_unlock(&mBC);
      currentCity = CITY_B;
    } else if(currentCity == CITY_D) {
      pthread_mutex_lock(&mBD);
      printf("Train %d is traveling from City D to City B.\n", (int) pthread_self());
      sleep(5);
      printf("Train %d is arrived at City B, the trip took 5 seconds.\n", (int) pthread_self());
      pthread_mutex_unlock(&mBD);
      currentCity = CITY_B;
    } else {
      printf("This city does not exist");
      pthread_exit(NULL);
    }
  }

  pthread_exit(NULL);
}
