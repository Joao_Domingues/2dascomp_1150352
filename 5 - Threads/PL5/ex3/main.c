#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <pthread.h>

typedef struct {
  int number;
  int index;
} data;

int array[1000];

void fillArray();
void * thread_func(void * args);

int main() {

  int i, result;
  void * retValue;
  data * data_1 = malloc(sizeof(*data_1));

  fillArray();

  printf("Insert a number to be searched\n");
  scanf("%d", &i);
  data_1->number = i;
  for(i = 0; i < 5; i++) {
    data_1->index = i;
    pthread_t thread_id;
    pthread_create(&thread_id, NULL, thread_func, data_1);
    pthread_join(thread_id, &retValue);
    if(retValue != NULL) printf("Thread number %d found the number\n", (int)retValue);
  }

  return 0;
}

void fillArray() {
  time_t t;
  srand ((unsigned) time (&t));
  int i, j, value, exists;

  for(i = 0; i < 1000; i++) {
    do {
      exists = 0;
      value = rand() % 1000;
      for(j = 0; j < i; j++) {
        if(array[j] == value) {
          exists = 1;
          break;
        }
      }
    } while(exists == 1);
    array[i] = value;
  }
}

void * thread_func(void * args) {

  data * data_1 = (data*) args;
  int i;
  for(i = 200 * data_1->index; i < 200 * (data_1->index + 1); i++) {
    if(array[i] == data_1->number) {
      printf("Found the value at position %d.\n", i);
      pthread_exit(&data_1->index);
    }
  }

  pthread_exit(NULL);
}
